#copy existing gdrive token
ln -sv ~/h/cfg/gdrive ~/.gdrive
#copy existing dropbox_uploader
ln -sv ~/h/cfg/dropbox_uploader ~/.dropbox_uploader

###gdrive
sudo mkdir -pv /opt/gdrive
sudo chown -v yakovcha:yakovcha /opt/gdrive
#download 
curl -L -k -o /opt/gdrive/gdrive-linux-386 "https://docs.google.com/uc?id=0B3X9GlR6EmbnLV92dHBpTkFhTEU&export=download"
chmod -v +x /opt/gdrive/gdrive-linux-386
sudo ln -sv /opt/gdrive/gdrive-linux-386 /usr/bin/gdrive

###dropbox_uploader
sudo mkdir -p /opt/dropbox_uploader
sudo chown yakovcha:yakovcha /opt/dropbox_uploader
curl "https://raw.githubusercontent.com/andreafabrizi/Dropbox-Uploader/master/dropbox_uploader.sh" -o /opt/dropbox_uploader/dropbox_uploader.sh
sudo ln -sv /opt/dropbox_uploader/dropbox_uploader.sh /usr/bin/dropbox_uploader
