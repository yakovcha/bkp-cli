ARCHIVE_NAME="backup_"`date +%Y-%m-%d_%H%M`

#backup-create.sh $ARCHIVE_NAME `find ~/h/stg/sha1 -type f -size -2M` ~/h/bin ~/h/cfg ~/h/txt ~/h/tmp
backup-create.sh $ARCHIVE_NAME `find ~/h/stg/sha1 -type f -size -1M` ~/h/bin ~/h/cfg ~/h/txt ~/h/tmp

gdrive upload /tmp/$ARCHIVE_NAME
bash dropbox_uploader upload /tmp/$ARCHIVE_NAME /

rm /tmp/$ARCHIVE_NAME

