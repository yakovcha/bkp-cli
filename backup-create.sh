ARCHIVE_NAME=$1
shift 1

log "archiving"
tar cvzf /tmp/$ARCHIVE_NAME.tar.gz $@
log "archive $ARCHIVE_NAME created"

du -h /tmp/$ARCHIVE_NAME.tar.gz

log "start encryption"
#gpg -c -o /tmp/$ARCHIVE_NAME --cipher-algo AES256 --symmetric /tmp/$ARCHIVE_NAME.tar.gz
# 1. decrypt save password with gpg and pass it to pype
# 2. -c -o encrypt to file
#    --passphrase-fd 0 --batch --yes   read password
#    last goes name of a archive to encrypt
gpg -d ~/h/cfg/my-backups-password.gpg | gpg -c -o /tmp/$ARCHIVE_NAME --passphrase-fd 0 --batch --yes /tmp/$ARCHIVE_NAME.tar.gz
log "encryption complete"

rm /tmp/$ARCHIVE_NAME.tar.gz
