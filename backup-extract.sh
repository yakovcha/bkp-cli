ARCHIVE_NAME=`printf $1 | cut -d '.' -f 1`
printf "%s\n" "$ARCHIVE_NAME"

log "decryption. waiting for passphrase."
gpg -d -o $ARCHIVE_NAME.tar.gz $ARCHIVE_NAME
log "finished decryption"

log "start extraction"
tar -xf $ARCHIVE_NAME.tar.gz
log "finished upacking"

rm $ARCHIVE_NAME.tar.gz

